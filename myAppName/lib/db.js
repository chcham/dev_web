var database = (function () {

    var questions = [{
            "id":"0",
            "domaine":"HTML",
            "question":"La balise <frame> a été:",
            "reponses" :["supprimée de HTML5",
                        "ajoutée au HTML5",
                        "conservée dans le HTML5",
                        "modifiée dans le HTML5"],
            "r":"1"
        },
        {
            "id":"1",
            "domaine":"HTML",
            "question":"Laquelle de ces balises n est pas une balise sémantique:",
            "reponses" :["section",
                        "article",
                        "footer",
                        "b"],
            "r":"4"
        },
		{
            "id":"2",
            "domaine":"HTML",
            "question":"div:",
            "reponses" :["est une balise sectionnante",
                        "met le texte en gras",
                        "représente un bloc",
                        "désigne un lien hypertext"],
            "r":"3"
        },
		{
            "id":"3",
            "domaine":"HTML",
            "question":"HTML signifie:",
            "reponses" :["Home Tool Markup Langage",
                        "HyperText Markup Link",
                        "HyperText Markup Langage",
                        "HyperLinks and Text Markup Langage"],
            "r":"3"
        },
		{

            "id":"4",
            "domaine":"HTML",
            "question":"Lequel de ces types n est pas un type de la balise \<input\>:",
            "reponses" :["form",
                        "color",
                        "date",
                        "number"],
            "r":"1"
        },
		{

            "id":"5",
            "domaine":"HTML",
            "question":"Les normes du HTML5 sont définis par: ",
            "reponses" :["Microsoft",
                        "The world wide web consortium",
                        "Chaque navigateur définit ces normes",
                        "Il n y a pas de normes"],
            "r":"2"
        },
		{

            "id":"6",
            "domaine":"HTML",
            "question":"En HTML une entête est déclarée par la balise:",
            "reponses" :["\<head\>",
                        "\<article\>",
                        "\<h1\>",
                        "\<strong\>"],
            "r":"1"
        },
		{

            "id":"7",
            "domaine":"HTML",
            "question":"En HTML le pied de page est défini par:",
            "reponses" :["\<end\>",
                        "\<footer\>",
                        "\<h6\>",
                        "\<h\>"],
            "r":"2"
        },
		{

            "id":"8",
            "domaine":"HTML",
            "question":"Un document HTML est obligatoirement composé:",
            "reponses" :["de head et body",
                        "de head et footer",
                        "de footer et body",
                        "de body"],
            "r":"1"
        },
		{

            "id":"9",
            "domaine":"HTML",
            "question":"Un texte important est déclaré avec la balise:",
            "reponses" :["\<i\>",
                        "\<b\>",
                        "\<strong\>",
                        "\<h1\>"],
            "r":"3"
        },
		{

            "id":"10",
            "domaine":"HTML",
            "question":"La balise \<a\> permet de:",
            "reponses" :["creer un hyperlien",
                        "mettre le texte en gras",
                        "mettre le texte en italique",
                        "souligner le texte"],
            "r":"1"
        },
		{

            "id":"11",
            "domaine":"HTML",
            "question":"Pour fermer une balise les symboles utilisés sont:",
            "reponses" :["*\>",
                        "\/\>",
                        "&\>",
                        "-\>"],
            "r":"2"
        },
		{

            "id":"12",
            "domaine":"HTML",
            "question":"Une liste non numérotée est créée avec la balise:",
            "reponses" :["\<ml\>",
                        "\<nl\>",
                        "\<el\>",
                        "\<ul\>"],
            "r":"4"
        },
		{

            "id":"13",
            "domaine":"HTML",
            "question":"La balise pour insérer une image est:",
            "reponses" :["<img src =\"chemin image\"\>",
                        "\<img url=\"chemin image\"\>",
                        "\<img alt=\"chemin image\"\>",
                        "\<img href=\"chemin image\"\>"],
            "r":"1"
        },
		{

            "id":"14",
            "domaine":"HTML",
            "question":"Les commentaires HTML s ecrivent:",
            "reponses" :["\/*...*\/",
                        "\<*...*\>",
                        "\<!--...--\>",
                        "\/...\/"],
            "r":"3"
        },
		{

            "id":"15",
            "domaine":"HTML",
            "question":"le titre d un document HTML est défini par la balise:",
            "reponses" :["\<titre\>",
                        "\<title\>",
                        "\<head\>",
                        "\<meta\>"],
            "r":"2"
        },
		{

            "id":"16",
            "domaine":"HTML",
            "question":"Au début de la page HTML il est nécéssaire d insérer:",
            "reponses" :["\<!DOCTYPE html\> ",
                        "\<!DOCTYPE\>",
                        "\<!DOCTYPE HTML5\>",
                        "\<DOCTYPE\>"],
            "r":"1"
        },
		{

            "id":"17",
            "domaine":"HTML",
            "question":"Quelle balise permet de créer un menu de navigation:",
            "reponses" :["\<nav\>",
                        "\<navigate\>",
                        "\<navigation\>",
                        "\<n\>"],
            "r":"1"
        },{

            "id":"18",
            "domaine":"HTML",
            "question":"L entête d un bloc est défini par:",
            "reponses" :["\< head \>",
                        "\< header \>",
                        "\< title \>",
                        "\< head title \>"],
            "r":"2"
        },{

            "id":"19",
            "domaine":"HTML",
            "question":"La balise permettant de créer un paragraphe est:",
            "reponses" :["\<p\>",
                        "\<b\>",
                        "\<paragraph\>",
                        "\<block\>"],
            "r":"1"
        },{

            "id":"20",
            "domaine":"CSS",
            "question":"CSS signifie:",
            "reponses" :["Cascade Style Sheets",
                        "Cascading Style Sheets",
                        "Collection Style Sheets",
                        "Colection Style Services"],
            "r":"2"
        },
		{

            "id":"21",
            "domaine":"CSS",
            "question":"Un commentaire CSS est défini par:",
            "reponses" :["\/*...*\/",
                        "\/\/...",
                        " \<!--...--\>",
                        "\/...\/"],
            "r":"1"
        },
		{

            "id":"22",
            "domaine":"CSS",
            "question":"Un attribut non reconnu:",
            "reponses" :["est ignoré",
                        "déclenche une erreur",
                        "empeche l affichage de la page",
                        "fait disparaitre le bloc le contenant"],
            "r":"1"
        },
		{

            "id":"23",
            "domaine":"CSS",
            "question":"Le sélecteur universel est:",
            "reponses" :["rien",
                        " *",
                        "\/",
                        "..."],
            "r":"2"
        },
		{

            "id":"24",
            "domaine":"CSS",
            "question":"'Le sélecteur par identificateur:",
            "reponses" :["peut être multiple dans un document",
                        "est unique",
                        "ne peut être utilisé que 2 fois",
                        "ne doit jamais être utlisé"],
            "r":"2"
        },
		{

            "id":"25",
            "domaine":"CSS",
            "question":"Le sélecteur par identificateur id a pour forme:",
            "reponses" :["id",
                        ".id",
                        "\/id",
                        "#id"],
            "r":"4"
        },
		{

            "id":"26",
            "domaine":"CSS",
            "question":"Un selecteur par classe a pour forme:",
            "reponses" :[".classe",
                        "#classe",
                        "classe",
                        "\/classe"],
            "r":"1"
        },
		{

            "id":"27",
            "domaine":"CSS",
            "question":"Le symbole \> permet de:",
            "reponses" :["spécifier un parent",
                        "spécifier un sélecteur moins important",
                        "spécifier un enfant direct",
                        " spécifier un enfant indirect"],
            "r":"3"
        },
		{

            "id":"28",
            "domaine":"CSS",
            "question":"Question a ajouter",
            "reponses" :["a",
                        "b",
                        "c",
                        "d"],
            "r":"3"
        },
		{

            "id":"29",
            "domaine":"CSS",
            "question":"La forme elem1~elem2 sélectionne:",
            "reponses" :["tous les élements elem2",
                        "tous les élements elem1",
                        "l élément elem2 situé après elem1 et au même niveau",
                        "l element elem2 situé après elem1 et étant un enfant direct de elem1"],
            "r":"3"
        },
		{

            "id":"30",
            "domaine":"CSS",
            "question":"Autre question a ajouter",
            "reponses" :["a",
                        "b",
                        "c",
                        "d"],
            "r":"3"
        },
		{

            "id":"31",
            "domaine":"CSS",
            "question":"La forme elem1+elem2 sélectionne:",
            "reponses" :["l element elem2 situé après elem1 au même niveau",
                        "l element elem2 situé après elem1 au niveau inférieur",
                        "l element elem2 situé juste après elem1 au même niveau",
                        "l element elem2 situé juste après elem1 au niveau inférieur"],
            "r":"2"
        },

		{

            "id":"32",
            "domaine":"JavaScript",
            "question":"Lequel de ces types de données n est pas un type de données de Javascript:",
            "reponses" :["float",
                        "nombre",
                        "string",
                        "boolean"],
            "r":"1"
        },
		{

            "id":"33",
            "domaine":"JavaScript",
            "question":"Il est possible en Javascript de réaliser des conversions automatique:",
            "reponses" :["Seulement dans certains cas",
                        "Oui",
                        "Non",
                        "Je ne sais pas"],
            "r":"1"
        },
		{

            "id":"34",
            "domaine":"JavaScript",
            "question":"Lors d une comparaison booléenne undefined est égal à:",
            "reponses" :["True",
                        "False",
                        "Aucunes de ces deux réponses",
                        "Je ne sais pas"],
            "r":"2"
        },
		{

            "id":"35",
            "domaine":"JavaScript",
            "question":"Une variable déclarée avec le mot clé var est:",
            "reponses" :["un pointeur",
                        "une variable globale",
                        "une variable constante",
                        "une variable locale"],
            "r":"4"
        },
		{

            "id":"36",
            "domaine":"JavaScript",
            "question":"Une fonction doit toujours retourner une valeur:",
            "reponses" :["  Vrai, un nombre par défaut",
                        "Vrai, la valeur undefined par defaut",
                        "Vrai, un booléen par défaut",
                        "Faux"],
            "r":"2"
        },
		{

            "id":"37",
            "domaine":"JavaScript",
            "question":"Question JS",
            "reponses" :["a",
                        "b",
                        "c",
                        "d"],
            "r":"3"
        },
		{

            "id":"38",
            "domaine":"JavaScript",
            "question":"Une expresion de fonction peut être placé dans une variable:",
            "reponses" :["Vrai mais seulement pour les fonctions dont le retour est un booléen",
                        " Vrai mais seulement pour les fonctions dont le retour est un nombre",
                        "Vrai pour tous",
                        "Faux"],
            "r":"2"
        },
				{

            "id":"39",
            "domaine":"JavaScript",
            "question":"Quel est le rapport entre le Java et le Javascript:",
            "reponses" :["Ce sont les mêmes langage",
                        "Ce sont deux langages relativement différent",
                        "Aucunes de ces réponses",
                        "Je ne sais pas"],
            "r":"2"

    }];

    function getQuestion(id){
      return questions[id];
    }

    function getNbQuestionsMax(){
      return questions.length;
    }

    return{
      getQuestion: getQuestion,
      getNbQuestionsMax: getNbQuestionsMax
    };

})();

module.exports = database;
