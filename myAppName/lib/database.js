var database = (function () {

    var questions = [{
            "id":"0",
            "domaine":"HTML",
            "question":"Quelle est la derniere version de HTML?",
            "reponses" :["HTML 2.0",
                        "HTML 3.0",
                        "HTML 4.0",
                        "HTML 5.0"],
            "r":"3"
        },
        {
            "id":"1",
            "domaine":"HTML",
            "question":"Le HTML est un langage dit:",
            "reponses" :["Encodé",
                        "Crypté",
                        "Balisé",
                        "Français"],
            "r":"2"
        },
        {
            "id":"2",
            "domaine":"HTML",
            "question":"Quelle est la balise utilisée pour les paragraphes?",
            "reponses" : ["p",
                         "a",
                         "div",
                         "nav"],
            "r":"0"
        },
        {
            "id":"3",
            "domaine":"HTML",
            "question":"Que signifie HTML?",
            "reponses":["Hyper Text Markup Language",
                       "Hello To My Loulou",
                       "Hyper Tools Markup Language",
                       "Hibou Terrestre Modifié Lentement"],
            "r":"0"
        },
        {
            "id":"4",
            "domaine":"HTML",
            "question":"A quoi sert la balise br",
            "reponses":["Définir un saut de ligne",
                       "Afficher une ligne horizontale",
                       "Afficher une ligne verticale",
                       "Afficher une boite"],
            "r":"0"
        },
        {
            "id":"5",
            "domaine":"CSS",
            "question":"Quelle est la dernière version de CSS?",
            "reponses":["CSS 1.0",
                       "CSS 2.0",
                       "CSS 3.0",
                       "CSS 4.0"],
            "r":"2"
        },
        {
            "id":"6",
            "domaine":"CSS",
            "question":"Que signifie CSS?",
            "reponses":["Cascading Style Sheets",
                       "Create Simple Samples",
                       "Choucroute Saucisse Saumon",
                       "Coordinate Style Sheets"],
            "r":"0"
        },
        {
            "id":"7",
            "domaine":"CSS",
            "question":"A quoi sert le langage CSS?",
            "reponses":["Réaliser des pages dynamiques",
                        "Mettre en forme les pages HTML",
                        "Insérer du contenu dans une page internet",
                        "Parler à un coq"],
            "r":"1"
        },
        {
            "id":"8",
            "domaine":"CSS",
            "question":"Quelle balise est utilisée pour insérer le CSS dans une page HTML?",
            "reponses":["La balise css",
                       "La balise style" ,
                       "La balise link",
                       "La balise url"],
            "r":"2"
        },
        {
            "id":"9",
            "domaine":"CSS",
            "question":"Quelle est la structure d'une déclaration CSS?",
            "reponses":["Des balises",
                       "Des scripts",
                       "Des petits pois",
                       "Des propriétés avec des valeurs"],
            "r":"3"
        },
        {
            "id":"10",
            "domaine":"Javascript",
            "question":"Combien de types de données possède le Javascript?",
            "reponses":["1",
                       "3",
                       "5",
                       "6"],
            "r":"3"

    }];

    function getQuestion(id){
      return questions[id];
    }

    function getNbQuestionsMax(){
      return questions.length;
    }

    /*function getQuestionByDomain(domaine){
      var str = JSON.stringify(questions);
      var obj_json = JSON.parse(str);
      console.log(obj_json);
      console.log(obj_json.domaine);

    }*/

    return{
      getQuestion: getQuestion,
      getNbQuestionsMax: getNbQuestionsMax
      //getQuestionByDomain: getQuestionByDomain
    };

})();

module.exports = database;
