var express = require('express');
var bodyParser = require('body-parser');
var database = require('../lib/database');
var router = express.Router();


router.get('/Test', function(req, res, next) {
    res.render('Test', {title: "Test rapide"});
});

router.post('/Test', function(req, res, next){
  // va récupérer une question dans la base de donnée au format Json
  var question = database.getQuestion(questionAleatoire(database.getNbQuestionsMax()));
  // retourne le json de la question avec les réponses
  res.json(question);
});

function questionAleatoire(Max){
  return (Math.floor(Max*Math.random()+1));
}


module.exports = router;
