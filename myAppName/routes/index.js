var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('Accueil', {title: "Accueil"});
});

router.get('/Instruction', function(req, res, next) {
    res.render('Instruction', {title: "Instruction"});
});

/*router.get('/Tableau', function(req, res, next) {
    res.render('Tableau', {title: "Tableau de bord"});
});*/



/*router.get('/Test2', function(req, res, next) {
    res.render('Test2', {title: "Test rapide"});
});*/

/*router.get('/Examen', function(req, res, next) {
    res.render('Examen', {title: "Examen"});
});*/

router.get('/Examen2', function(req, res, next) {
    res.render('Examen2', {title: "Examen"});
});

router.get('/Fin_Examen', function(req, res, next) {
    res.render('Fin_Examen', {title: "Fin de l examen"});
});

module.exports = router;
