var express = require('express');
var bodyParser = require('body-parser');
var database = require('../lib/database');
var sessionStorage = require('sessionstorage');
var router = express.Router();


router.get('/Examen', function(req, res, next){
  res.render('Examen', {title: 'Examen'});
});

router.post('/Examen', function(req, res, next){
  // va récupérer une question dans la base de donnée au format Json
  //var question = database.getQuestion(questionAleatoire(database.getNbQuestionsMax()));
  //var idQuestion = getQuestionByDomain(sessionStorage.getItem("type").valueOf());
  //TODO: Trouver un moyen de recuperer le type avec sessionStorage?
  var idQuestion = getQuestionByDomain('HTML');
  var question = database.getQuestion(idQuestion);
  // retourne le json de la question avec les réponses
  res.json(question);
});


function getQuestionByDomain(domaine){
  for(;;){
    var alea = questionAleatoire(database.getNbQuestionsMax());
    var question = database.getQuestion(alea);

    var str = JSON.stringify(question);
    var obj = JSON.parse(str);

    if(obj.domaine == domaine){
      return alea;
    }
  }
}


function questionAleatoire(Max){
  return (Math.floor(Max*Math.random()+1));
}

module.exports = router;
