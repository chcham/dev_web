$(document).ready(function(){
  $("#btnTest").click(btnTestClickHandler);
  $("#btnExamen").click(btnExamenClickHandler);
  try {
	localStorage.getItem("nbTest").valueOf();
  }
  catch(e){
	localStorage.setItem("nbTest",0);
  }
  try {
	localStorage.getItem("nbTestReussi").valueOf();
  }
  catch(a){
	localStorage.setItem("nbTestReussi",0);
  }
  try {
	localStorage.getItem("nbNote").valueOf();
  }
  catch(a){
	localStorage.setItem("nbNote",0);
  }
  finally{

	 var res="Test rapides réussis : "+ localStorage.getItem("nbTestReussi").valueOf()+"/"+localStorage.getItem("nbTest").valueOf()+ " tests réalisés"+'</br>'+'</br>';
	 var moy=0;
	 var nb =localStorage.getItem("nbNote").valueOf();
	 for (var i=0; i<nb; i++)
	 {
		var key= "note["+i.valueOf()+"]";
		moy = moy +localStorage.getItem(key).valueOf();
	 }
	if (nb != 0){
		 moy= moy/nb;
	 }
	 res= res + "Moyenne obtenue aux examens: "+ moy+"/20";
	 $('#ResRapide').html(res);
  }

});

function btnTestClickHandler(event){
  //var nb= localStorage.getItem("nbTest").valueOf();
  //nb++;
  //localStorage.setItem("nbTest",nb);
  return true;
}

function btnExamenClickHandler(event){
  return true;
}


function saveData(nbQuest,type) {
	var numQuest=1;
	sessionStorage.setItem("nbQuest", nbQuest);
	sessionStorage.setItem("type", type);
	sessionStorage.setItem("numQuest",numQuest);
	sessionStorage.setItem("noteExam",0);
}



$(document).delegate('.bouton.sta', 'click', function() {
	var nb =localStorage.getItem("nbNote").valueOf();
	var note= "Notes obtenues aux examens:" +  '<br />'+'<br />' ;
	for (var i=0; i<nb; i++)
	{
		var type= "type["+i.valueOf()+"]";
		var key= "note["+i.valueOf()+"]";
		note = note +" "+ localStorage.getItem(key).valueOf()+"/20"+'<br />';
	}

	$("p#NotesObtenues").html(note);
});
