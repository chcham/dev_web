$(document).ready(function(){
	var num=sessionStorage.getItem("numQuest").valueOf();
	var nb=sessionStorage.getItem("nbQuest").valueOf();
	$("article header h1 #type").html(sessionStorage.getItem("type").valueOf());
	$("article header h1 #quest").html("Question "+num+ " / " + nb);

	if( num == nb){
		document.formulaire.action='/Fin_Examen';
	}
	$("#btnNext").click(btnNextClickHandler);
	postQuestionAndAnswers();
});

$(document).delegate('.quitte', 'click', function() {
	sessionStorage.setItem("noteExam",0);
});

function btnNextClickHandler(event){
  var num=sessionStorage.getItem("numQuest").valueOf();
  postQuestionAndAnswers();
  var chk = eval (document.formulaire.check1);
  for (i = 0; i < chk.length; i++)
  {
     if (chk[i].checked == true)
     {
			 var a=i;
			 a++
			 var idcheck= "span#r"+a;
			 if($(idcheck).hasClass("solution"))
			 {
				 alert("Bravo !!!");
				 var success=localStorage.getItem("noteExam").valueOf();
				 success++;
				 localStorage.setItem("noteExam",success);
			 }
     }
  }
  num++;
  sessionStorage.setItem("numQuest",num);
  return true;
}

function postQuestionAndAnswers(){
  $.ajax({
    url: '/Examen',
    type: 'POST',
    dataType: 'json',
    success: function(data){
      var str = JSON.stringify(data);
      var obj_json = JSON.parse(str);
      $('span#q').html(obj_json.question);
      // Possibilite d'utiliser une boucle?
      $('span#r1').html(obj_json.reponses[0]);
      $('span#r2').html(obj_json.reponses[1]);
      $('span#r3').html(obj_json.reponses[2]);
      $('span#r4').html(obj_json.reponses[3]);
		  var n=obj_json.r;
		  n++;
		  var sol= "span#r"+n;
		  $(sol).addClass("solution");
    }

  });
}

function allowDrop(ev){
  ev.preventDefault();
  $('span#'+ev.target.id).css('color', 'black');
}

function dragDebut(ev){
  ev.dataTransfer.setData("text", ev.target.id);

}

function drop(ev){
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  ev.target.appendChild(document.getElementById(data));
  // La réponse séectionnée se met en rouge
  var idSelect = ev.target.appendChild(document.getElementById(data)).id;
  $('span#'+idSelect).css('color', 'red');
  // On rend toutes les réponses indéplaçables une fois la réponse choisie
	// Fonctionne sur Firefox mais pas Chrome
	for(i=1; i <= 4; i++){
			$('span#r'+i).attr('draggable', 'false');
	}
  $('p#rep').html("Réponse choisie! Vous ne pouvez plus la modifier!");

	// Else if degueulass qui va sélectionner la checkbox
	// correspond à la réponse sélectionnée
	if(idSelect == 'r1'){
		$('input[type=checkbox][value=1]').prop('checked', true);
	}else if(idSelect == 'r2'){
		$('input[type=checkbox][value=2]').prop('checked', true);
	}else if(idSelect == 'r3'){
		$('input[type=checkbox][value=3]').prop('checked', true);
	}else if(idSelect == 'r4'){
		$('input[type=checkbox][value=4]').prop('checked', true);
	}else{
		alert("Invalide id");
	}


}

function dragFin(ev){

}
